import { Link } from "react-router-dom";
import { UserItemProps } from "./UserItem.props";

import "./UserItem.scss";

function UserItem({
  userRepos,
  userLogin,
  className = "",
  userName,
  userAvatar,
}: UserItemProps) {
  return (
    <Link to={`/user/${userLogin}`} className={`user ${className}`}>
      <img src={userAvatar} alt="" className="user__avatar" />
      <span className="user__name">{userName}</span>
      {userRepos?.length ? (
        <span className="user__repo">Repos: {userRepos.length}</span>
      ) : (
        <span className="user__repo">Repos: couldn't fetch info</span>
      )}
    </Link>
  );
}

export default UserItem;
