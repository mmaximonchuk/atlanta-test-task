import { DetailedHTMLProps, HTMLAttributes } from "react";
import { iRepo } from "../../domain/User";

export interface UserItemProps
  extends DetailedHTMLProps<
    HTMLAttributes<HTMLAnchorElement>,
    HTMLAnchorElement
  > {
    userLogin: string;
    className?: string;
    userName: string;
    userAvatar: string;
    userRepos: iRepo[];
  }
