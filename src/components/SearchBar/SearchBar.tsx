import { useEffect, useState } from "react";
import { Loader } from "../../components";
import { SearchBarProps } from "./SearchBar.props";

import './SearchBar.scss';

function SearchBar({
  type,
  className,
  onChangeInput,
  placeholder,
  name,
  value,
  isLoading
}: SearchBarProps) {
  const [searchTerm, setSearchTerm] = useState<string>(value);

  useEffect(() => {
    onChangeInput(searchTerm);
  }, [searchTerm])

  return (
    <div className={`search-bar ${className}`}>
      <input
        type={type}
        name={name}
        className="input-searchbar"
        placeholder={placeholder}
        onChange={(e) => setSearchTerm(e.target.value)}
        value={value}
      />
      {isLoading && <Loader className="search-bar__loader" />}
    </div>
  );
}

export default SearchBar;
