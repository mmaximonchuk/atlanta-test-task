import {
  DetailedHTMLProps,
  Dispatch,
  HTMLAttributes,
  SetStateAction,
} from "react";

export interface SearchBarProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLInputElement>, HTMLInputElement> {
  className?: string;
  value: string;
  name: string;
  placeholder: string;
  type: string;
  onChangeInput: Dispatch<SetStateAction<string>>;
  isLoading: boolean;
}
