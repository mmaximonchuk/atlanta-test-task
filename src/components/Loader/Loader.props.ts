import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface LoaderProps {
    className: string;
}