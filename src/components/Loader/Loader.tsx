import { LoaderProps } from './Loader.props';

import './Loader.scss';

function Loader({className, ...restProps}: LoaderProps) {
  return (
    <div className={`loader ${className}`} {...restProps}></div>
  )
}

export default Loader