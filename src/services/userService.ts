import { iError, iRepo, iUser, iUserDetails } from "../domain/User";

const basicUrl = "https://api.github.com";
const httpHeaders = {
  method: "GET",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/vnd.github.v3+json",
  },
};

type usersWithRepos = { users: iUser[]; userRepos: iRepo[][] };
type userDetailsWithRepos = { user: iUserDetails; userRepo: iRepo[] };
type responseWithError = { error: iError | null };

export const UsersAPI = {
  async getUsersWithRepos(): Promise<usersWithRepos & responseWithError> {
    const response = await fetch("https://api.github.com/users", httpHeaders);
    const data = await response.json();

    let users: iUser[] = [];
    let userRepos: iRepo[][] = [];
    let error = null;

    if (response.status == 403) {
      return { users, userRepos, error: data };
    }

    if (Array.isArray(users)) {
      users = data;
      userRepos = await Promise.all<iRepo[]>([
        ...users.map(async (user) => {
          const res = await fetch(
            `${basicUrl}/users/${user.login}/repos`,
            httpHeaders
          );
          return res.json();
        }),
      ]);
    } else {
      return { users, userRepos, error: data };
    }

    return { users, userRepos, error };
  },
  async getUserByName(
    userName: string
  ): Promise<userDetailsWithRepos & responseWithError> {
    let userRepo: iRepo[] = [];
    let user: iUserDetails = {} as iUserDetails;
    let error: iError | null = null;

    const response = await fetch(`${basicUrl}/users/${userName}`, httpHeaders);
    const data = await response.json();

    if (response.status == 403) {
      error = data;
    }

    user = data;
    const repoResponse = await fetch(
      `${basicUrl}/users/${user.login}/repos`,
      httpHeaders
    );

    userRepo = await repoResponse.json();

    return { user, userRepo, error };
  },
};
