import { DependencyList, EffectCallback, useEffect } from "react";

export const useDebouncedEffect = (
   effect: EffectCallback, 
   delay: number, 
   deps?: DependencyList
) => {
  useEffect(() => {
    const handlerId = setTimeout(() => effect(), delay);

    return () => clearTimeout(handlerId);
    
    // using || operator because 
    // if its optional then it can be undefined.
  }, [...(deps || []), delay]);
};
