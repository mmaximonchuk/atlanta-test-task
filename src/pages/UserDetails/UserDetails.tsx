import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { Loader, SearchBar } from "../../components";

// import { iError, iRepo, iUser, iUserDetails } from "../../domain/User";
import { UserDetailsProps } from "./UserDetails.props";

import "./UserDetails.scss";

function UserDetails({ getUserByName, repoName, setRepoName, userRepos, userData, error }: UserDetailsProps): JSX.Element {
  const { login } = useParams();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    if (login) {
      getUserByName(login, setIsLoading);
    }
  }, [login]);

  const filteredRepos = repoName.length
    ? userRepos[0].filter((repo) => repo.name.includes(repoName.toLowerCase()))
    : userRepos[0]


  const showDetails =
    !error && !isLoading && userData;
  const showLoader = isLoading;
  const showError = error !== null;

  return (
    <div className="user-details-page">
      {showLoader && <Loader className="user-details-page__loader" />}
      {showError && <p className="error">{error.message}</p>}
      {showDetails && (
        <>
          <div className="user-details">
            <div className="user-avatar">
              <img
                className="user-picture"
                src={userData.avatar_url}
                alt=""
              />
            </div>
            <div className="user-data">
              <p>{userData.login}</p>
              <p>{userData.email}</p>
              <p>{userData.location}</p>
              {/* <p>
                {userData["created_at"].getDay()}.
                {userData["created_at"].getMonth()}.
                {userData["created_at"].getFullYear()}
              </p> */}
              <p>{userData.followers} Followers</p>
              <p>Following {userData.following}</p>
            </div>
          </div>
          <p className="user-biography">{userData.bio}</p>
          <SearchBar
            placeholder="Search for User's Repository"
            name="username"
            type="text"
            onChangeInput={setRepoName}
            value={repoName}
            isLoading={isLoading}
          />
          <div className="repo-list">
            {userRepos?.length &&
              filteredRepos?.map((repo) => {
                return (
                  <a target="_blank" rel="noopener noreferrer" href={repo.git_url.replace(/(git:)/, 'https:')} key={repo.id} className="repo">
                    <span>{repo.name}</span>
                    <div className="repo__details">
                      <span>Forks: {repo.forks}</span>
                      <span>Stars: {repo.stargazers_count}</span>
                    </div>
                  </a>
                );
              })}
          </div>
        </>
      )}
    </div>
  );
}

export default UserDetails;
