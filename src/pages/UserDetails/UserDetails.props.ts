import { iError, iRepo, iUserDetails } from "../../domain/User";

export interface UserDetailsProps {
  error: iError | null;
  repoName: string;
  getUserByName: (
    name: string,
    setIsLoading: React.Dispatch<React.SetStateAction<boolean>>
  ) => void;
  setRepoName: React.Dispatch<React.SetStateAction<string>>;
  isLoading: boolean;
  userData: iUserDetails;
  userRepos: iRepo[][];
}
