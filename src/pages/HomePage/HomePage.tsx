import { Loader, SearchBar, UserItem } from "../../components";
import { iUserDetails } from "../../domain/User";
import { HomaPageProps } from "./HomePage.props";

import "./HomePage.scss";

function HomePage({ users, usersRepos, userRepos: foundUserRepos, error, userNameSearchTerm, setUserNameSearchTerm, isLoading }: HomaPageProps): JSX.Element {

  const showLoader = isLoading;
  const showUsers =
   !error && Array.isArray(users) && users?.length !== 0 && !showLoader;
  const showError = error !== null && Boolean(error?.message);
  const showEmptyUsers = Boolean(!error && !showLoader && Array.isArray(users) && users?.length === 0);

  return (
    <div className="home-page">
      <SearchBar
        placeholder="Search for Users"
        name="username"
        type="text"
        onChangeInput={setUserNameSearchTerm}
        value={userNameSearchTerm}
        isLoading={isLoading}
      />
      <div className="user-list">
        {showUsers ? users.map((user: iUserDetails, index: number) => {
          const userRepos = userNameSearchTerm.length > 0 ? foundUserRepos[index] : usersRepos[index];

          return (
            <UserItem
              key={user.id}
              userLogin={user.login}
              userName={user.login}
              userAvatar={user.avatar_url}
              userRepos={userRepos}
            />
          );
        }) : null}

        {showLoader && <Loader className="home-page__loader" />}
        {showEmptyUsers && <p className="warning">There are no users with this username :(</p>}
        {showError ? <p className="error">{error.message}</p> : null}
      </div>
    </div>
  );
}

export default HomePage;
