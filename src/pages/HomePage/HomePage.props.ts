import { iError, iRepo, iUser } from "../../domain/User";

export interface HomaPageProps {
  users: iUser[];
  usersRepos: iRepo[][];
  userRepos: iRepo[][];
  error: iError | null;
  userNameSearchTerm: string;
  setUserNameSearchTerm: React.Dispatch<React.SetStateAction<string>>;
  isLoading: boolean;
}
