import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import { AppRoutes } from "./routes";
import { UsersAPI } from "./services/userService";
import { useDebouncedEffect } from "./hooks/useDebounce";

import { iError, iRepo, iUser, iUserDetails } from "./domain/User";
import { HomePage, UserDetails } from "./pages";

import "./App.scss";

function App() {
  const [users, setUsers] = useState<iUser[]>([]);
  const [usersRepos, setUsersRepos] = useState<iRepo[][]>([]);
  const [userNameSearchTerm, setUserNameSearchTerm] = useState<string>("");
  const [repoName, setRepoName] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<iError | null>(null);

  const [userData, setUserData] = useState<iUserDetails>({} as iUserDetails);
  const [userRepos, setUserRepos] = useState<iRepo[][]>([]);

  const getUsers = async () => {
    try {
      setError(null);
      setIsLoading(true);
      const { users, userRepos: repos, error } = await UsersAPI.getUsersWithRepos();

      // type response = ReturnType<T extends (...users: iUser[]) => iUser>;

      if (error !== null) {
        setError(error);
        return;
      }

      setUsers(users);
      setUsersRepos(repos);

    } catch (err) {
      const error = err as iError;
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  const getUserByName = async (name: string, setIsLoading: React.Dispatch<React.SetStateAction<boolean>>) => {

    try {
      setError(null);
      setIsLoading(true);
      const { user, userRepo, error } = await UsersAPI.getUserByName(name);

      if (error !== null) {
        setError(error);
        return;
      }

      // if (!user.hasOwnProperty("login")) {
      //   let error = user as iError;
      //   setError(error);
      // }

      const userInArray: iUserDetails[] = [user as iUserDetails];
      const repoInArray: iRepo[][] = [userRepo];

      setUsers(userInArray);
      setUserRepos(repoInArray);

      setUserData(user);
      setUserRepos(repoInArray);
    } catch (err) {
      const error = err as iError;
      setError(error);
    } finally {
      setIsLoading(false);
    }
  };

  useDebouncedEffect(() => {
    if (userNameSearchTerm.length) {
      getUserByName(userNameSearchTerm, setIsLoading);
    } else {
      getUsers();
    }
  }, 1000, [userNameSearchTerm]);


  useEffect(() => {
    getUsers();
  }, []);

  return (
    <Router>
      <div className="App">
        <header className="header">
          <p className="header__title">GitHub Searcher</p>
        </header>
        <Routes>
          <Route
            path={AppRoutes.HomePage}
            element={
              <HomePage
                users={users}
                usersRepos={usersRepos}
                userRepos={userRepos}
                error={error}
                userNameSearchTerm={userNameSearchTerm}
                setUserNameSearchTerm={setUserNameSearchTerm}
                isLoading={isLoading}
              />
            }
          />
          <Route
            path={AppRoutes.UserDatails}
            element={
              <UserDetails
                error={error}
                repoName={repoName}
                setRepoName={setRepoName}
                isLoading={isLoading}
                getUserByName={getUserByName}
                userData={userData}
                userRepos={userRepos}
              />
            }
          />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
